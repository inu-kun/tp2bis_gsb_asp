﻿<%@ Page Title="" Language="C#" MasterPageFile="main.Master" AutoEventWireup="true" CodeBehind="seConnecter.aspx.cs" Inherits="TP1_GSB_Authentification.seConnecter" %>
<%@ Import namespace="TP1_GSB_Authentification.mesClasses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        Cvisiteurs ovisiteur = new Cvisiteurs();
        string id = Request.Form["email"];
        string mdp = Request.Form["pwd"];
        if (id != "" && mdp != "" && id != null && mdp != null)
        {
            
           Cvisiteur ovisit = ovisiteur.getVisiteur(id, mdp);

           if (ovisit != null)
        {
            Session["prenom"] = ovisit.prenom_visiteur;
            Session["id"] = ovisit.id_visiteur;
            Response.Redirect("home.aspx");
        }
        }
        
         %>
    <div class="container">
  <form class="form-horizontal" method="post" action="seConnecter.aspx">
    <div class="form-group">
      <label class="control-label col-sm-2 col-xs-2" for="email">Email:</label>
      <div class="col-sm-3 col-xs-3">
        <input type="text" class="form-control" id="email" placeholder="Entrer email" name="email"/>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2 col-xs-2" for="pwd">Mot de passe:</label>
      <div class="col-sm-3 col-xs-3">          
        <input type="password" class="form-control" id="pwd" placeholder="Entrer mot de passe" name="pwd"/>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10 col-xs-offset-2 col-xs-10">
        <div class="checkbox">
          <label><input type="checkbox" name="remember"/> Se rappeler de moi</label>
        </div>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10 col-xs-offset-2 col-xs-10">
        <button type="submit" class="btn btn-default" name="btn_login">Envoyer</button>
      </div>
    </div>
  </form>
</div>
            </body>
    <footer class="footer text-primary">
    Construit pour TP isi1-web - 2017 -.
</footer>
</asp:Content>