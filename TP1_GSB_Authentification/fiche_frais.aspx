﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master"  AutoEventWireup="true" CodeBehind="fiche_frais.aspx.cs" Inherits="TP1_GSB_Authentification.fiche_frais" %>
<%@ Import namespace="TP1_GSB_Authentification.mesClasses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    




    <%
        string libelle = Request.Form["libelle"];
        string idETP = Request.Form["idETP"];
        int id = Convert.ToInt32(Request.Form["id"]);

        if (libelle != null || idETP!=null || id!=0)
        {

            int number = Convert.ToInt32(Request.Form["number"]);

            int ETP = Convert.ToInt32(Request.Form["ETP"]);

            int KM = Convert.ToInt32(Request.Form["KM"]);
            string idKM = Request.Form["idKM"];
            int NUI = Convert.ToInt32(Request.Form["NUI"]);
            string idNUI = Request.Form["idNUI"];
            int REP = Convert.ToInt32(Request.Form["REP"]);
            string idREP = Request.Form["idREP"];
            string idf = Request.Form["idf"];



            if (id != 0){
                CligneFaisHorsForfaits delet = new CligneFaisHorsForfaits();
                delet.deletFHF(id);
                //header("location:fiche_frais.php");

            }

            if(number != 0 && libelle != null){
                CligneFaisHorsForfaits insert = new CligneFaisHorsForfaits();
                insert.insertFHF(libelle, number, Convert.ToString(Session["id"]));
                //header("location:fiche_frais.php");
            }

            if(ETP != 0){
                CligneFraisForfait insert = new CligneFraisForfait();
                insert.insertLigneFraisForfait(idETP, Convert.ToString(Session["id"]), ETP );
                //header("location:fiche_frais.php");
            }
            if(KM != 0){
                CligneFraisForfait insert = new CligneFraisForfait();
                insert.insertLigneFraisForfait(idKM, Convert.ToString(Session["id"]), KM);
                //header("location:fiche_frais.php");
            }
            if(NUI != 0){
                CligneFraisForfait insert = new CligneFraisForfait();
                insert.insertLigneFraisForfait(idNUI,Convert.ToString(Session["id"]), NUI);
                //header("location:fiche_frais.php");
            }
            if(REP != 0){
                CligneFraisForfait insert = new CligneFraisForfait();
                insert.insertLigneFraisForfait(idREP,Convert.ToString(Session["id"]), REP);
                //header("location:fiche_frais.php");
            }
            if(idf != null){
                CligneFraisForfait delet = new CligneFraisForfait();
                delet.deletFraisForfait(idf);
                //header("location:fiche_frais.php");
            }
        }

        CligneFaisHorsForfaits oLigneFaisHorsForfaits = new CligneFaisHorsForfaits();
        CligneFraisForfait oLigneFraisForfait = new CligneFraisForfait();
        MySql.Data.MySqlClient.MySqlDataReader fraiForfait = oLigneFraisForfait.getFraisForfait();
        //MySql.Data.MySqlClient.MySqlDataReader lff= connectionff.getLigneFraisForfait();
        //MySql.Data.MySqlClient.MySqlDataReader lff2= connectionff.getLigneFraisForfait();
        Dictionary<string, CligneFaisHorsForfait> oDicoLigneFraisHorsForfait = oLigneFaisHorsForfaits.getFHF(Convert.ToString(Session["id"]));
        CficheFraiss oVerif=new CficheFraiss();
        oVerif.VerificationFicheFrais(Convert.ToString(Session["id"]));/* */

         %>
    
    
    
    
    
    
    
    <head>
        <!------------- DEBUT FFF !----------------->
<title>Fiche Frais</title>
<div class="container">
<h1><u>SAISIE FICHE FRAIS  FORFAIT</u></h1>
<form class="form-horizontal" method="post" action="fiche_frais.aspx">
<%

 while (fraiForfait.Read())
 {
     Response.Write("" +
         "<div class=\"form-group\">\r\n" +
         "<label class=\"control-label col-sm2 col-xs-2\" for=\"etape\">" + fraiForfait["libelle"] + ":</label>\r\n" +
         "<div class=\"col-sm-3 col-xs-3\">\r\n" +
         "<input type=\"number\" class=\"form-control\" name=\"" + fraiForfait["id"] + "\" id=\"" + fraiForfait["id"] + "\" >\r\n" +
         "<input type=\"hidden\" name=\"id" + fraiForfait["id"] + "\" id=\"id" + fraiForfait["id"] + "\" value=\"" + fraiForfait["id"] + "\">\r\n" +
         "</div>\r\n" +
         "<div class=\"col-sm-4 col-xs-4\">\r\n" +
         "<input type=\"text\" value=\"" + fraiForfait["montant"] + "\" disabled=\"disabled\"  >\r\n" +
         "</div>\r\n" +
         "</div>\r\n" );
 }%> 
         <div class="form-group">
         <div class="col-sm-offset-2 col-sm-10 col-xs-offset-2 col-xs-10">
         <button type="submit" class="btn btn-success">Envoyer</button>
         </div>
         </div>
         </form>
         </div>
        <!------------- DEBUT FHF !----------------->
        <div class="container">
<h1><u>SAISIE FICHE FRAIS HORS FORFAIT</u></h1>
  <form class="form-horizontal" method="post" action="fiche_frais.aspx">
    <div class="form-group">
      <label class="control-label col-sm-2 col-xs-2" for="libelle">Libelle:</label>
      <div class="col-sm-3 col-xs-3">
        <textarea rows="4" cols="50" name="libelle" id="libelle" ></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2 col-xs-2" for="pwd">Montant:</label>
      <div class="col-sm-3 col-xs-3">          
        <input type="number" class="form-control" id="number" name="number" min="1"/>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10 col-xs-offset-2 col-xs-10">
        <button type="submit" class="btn btn-success">Envoyer</button>
      </div>
    </div>
  </form>
</div>
<div class="container">
  <h2><u>Recapitulatif des frais hors forfait du mois</u></h2>         
  <table class="table table-hover">
  
  
    <thead>
      <tr>
        <th>libelle</th>
        <th>date</th>
        <th>montant</th>
		<th>action</th>
      </tr>
    </thead>
    <tbody>
      <%
          if (oDicoLigneFraisHorsForfait != null)

          {
              int i = 0;

              foreach (KeyValuePair<string, CligneFaisHorsForfait> t in oDicoLigneFraisHorsForfait)
              {
                  if (i == 0)
                  {
                      Response.Write("" +
               "<tr>" +
                   "<form  action=\"fiche_frais.aspx\" method=\"post\">" +
                   "<td><input type=\"hidden\" id=\"id\" name=\"id\" value=\"" + t.Value.id + "\"/>" + t.Value.libelle + "</td>" +
                   "<td>" + t.Value.date + "</td>" +
                   "<td>" /*/if (Convert.ToInt32(t.Value.montant) >= 100) { Response.Write("<p style=\"color:red;\">" + t.Value.montant + "</p>"); } else {*/ + t.Value.montant + "</p> </td>" +
                    "<td><button type=\"submit\"  class=\"btn btn-danger\">supprimer</button></td>" +
                    "</form>" +
                "</tr>");
                      
                  }
                  i++;
                  if(i==4)
                  {
                      i = 0;
                  }
                  
              }
          }

              %>
    </tbody>
  </table>
</div>
    </asp:Content>
