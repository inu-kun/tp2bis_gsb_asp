﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using TP1_GSB_Authentification.mesClasses.b;
namespace TP1_GSB_Authentification.mesClasses
{
    public class CligneFaisHorsForfait
    {
        public int id { get; set; }
        public string idVisiteur { get; set; }
        public string mois { get; set; }
        public string libelle { get; set; }
        public DateTime date { get; set; }
        public int montant { get; set; }

        public CligneFaisHorsForfait(int sid, string sidVisiteur, string smois, string slibelle, DateTime sdate, int smontant)
        {
            id = sid;
            idVisiteur = sidVisiteur;
            mois = smois;
            libelle = slibelle;
            date = sdate;
            montant = smontant;
        }
    }

    public class CligneFaisHorsForfaits
    {
        public Dictionary<string, CligneFaisHorsForfait> ocollDicoLigneFaisHorsForfait;

        

       public CligneFaisHorsForfaits()
        {
            /*ocollDicoLigneFaisHorsForfait = new Dictionary<string, CligneFaisHorsForfait>();
            Cdao odao = new Cdao();
            string query = "SELECT * FROM `lignefraishorsforfait` WHERE idVisiteur='"+id+"'";
            MySqlDataReader ord = odao.getReader(query);
            //Cvisiteur ovisiteur  = ocollDicovisit["toto"];
            while (ord.Read())
            {
                CligneFaisHorsForfait oligneFaisHorsForfait = new CligneFaisHorsForfait(Convert.ToInt32(ord["id"]), Convert.ToString(ord["idVisiteur"]), Convert.ToString(ord["mois"]), Convert.ToString(ord["libelle"]), Convert.ToDateTime(ord["date"]), Convert.ToInt32(ord["montant"]));
                ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["id"]), oligneFaisHorsForfait);
                ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["idVisiteur"])+Convert.ToString(ord["id"]), oligneFaisHorsForfait);
                ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["mois"])+ Convert.ToString(ord["id"]), oligneFaisHorsForfait);
                ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["libelle"]), oligneFaisHorsForfait);
                ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["date"]), oligneFaisHorsForfait);
                ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["montant"]), oligneFaisHorsForfait);
            }*/
        }
       public Dictionary<string, CligneFaisHorsForfait> getFHF (string id)
        {
            string date = Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Month); //date("Y").date("m");
            ocollDicoLigneFaisHorsForfait = new Dictionary<string, CligneFaisHorsForfait>();
            Cdao odao = new Cdao();
            string query = "SELECT * FROM `lignefraishorsforfait` WHERE idVisiteur='" + id + "' AND mois='"+date+"'";
            MySqlDataReader ord = odao.getReader(query);
            CligneFaisHorsForfait oligneFaisHorsForfait;
            //Cvisiteur ovisiteur  = ocollDicovisit["toto"];
        while (ord.Read())
        {
            oligneFaisHorsForfait = new CligneFaisHorsForfait(Convert.ToInt32(ord["id"]), Convert.ToString(ord["idVisiteur"]), Convert.ToString(ord["mois"]), Convert.ToString(ord["libelle"]), Convert.ToDateTime(ord["date"]), Convert.ToInt32(ord["montant"]));
            ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["id"]), oligneFaisHorsForfait);
            //ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["idVisiteur"]) + Convert.ToString(ord["id"]), oligneFaisHorsForfait);
            //ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["mois"]) + Convert.ToString(ord["id"]), oligneFaisHorsForfait);
            ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["libelle"]), oligneFaisHorsForfait);
            ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["date"]), oligneFaisHorsForfait);
            ocollDicoLigneFaisHorsForfait.Add(Convert.ToString(ord["montant"]), oligneFaisHorsForfait);
        }            
                return ocollDicoLigneFaisHorsForfait;
            
        }

            public string insertFHF (string libelle, int number, string id)
        {
         string date = Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Month);
         string req ="INSERT INTO lignefraishorsforfait (idVisiteur, mois,  libelle, montant, date) VALUES ('"+id+"', '"+date+"',  '"+libelle+"', '"+number+"', now())"; 
         Cdao insertfhf = new Cdao();
		 insertfhf.insertEnreg(req);
		 return null;
        }

        public string deletFHF (int id)
        {
    	string req ="DELETE FROM lignefraishorsforfait WHERE id='"+id+"'";
        Cdao deletfhf = new Cdao();
		deletfhf.deleteEnreg(req);
		return null;
        }
    }
}