﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using TP1_GSB_Authentification.mesClasses.b;

namespace TP1_GSB_Authentification.mesClasses
{
    public class CligneFraisForfait
    {
        public MySqlDataReader getFraisForfait()
        {
	    string req = "SELECT * FROM fraisforfait";
        Cdao odao = new Cdao();
		MySqlDataReader ord=odao.getReader(req);
		return ord;
        }


        /*public MySqlDataReader getLigneFraisForfait()
        {
            string req = "SELECT * FROM lignefraisforfait";
            Cdao odao = new Cdao();
            MySqlDataReader recu = odao.getReader(req);
            CligneFraisForfait ff = new CligneFraisForfait();
            return null;
        }*/


        public MySqlDataReader insertLigneFraisForfait(string id, string idv, int quantite)
	    {
            string date = Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Month);
            string req ="SELECT mois, idFraisForfait FROM lignefraisforfait WHERE idFraisForfait='"+id+"'AND mois='"+date+"'";
		    Cdao odao = new Cdao();
		    MySqlDataReader ord = odao.getReader(req);
            if (ord.Read())
            {
                string mois = Convert.ToString(ord["mois"]);
                string idF = Convert.ToString(ord["idFraisForfait"]);
                if (date == mois && id == idF)
                {
                    CligneFraisForfait maj = new CligneFraisForfait();
                    maj.updateFraisForfait(id, idv, quantite);
                }
            }
            else
		    {
			string req2 ="INSERT INTO lignefraisforfait (idVisiteur, mois,  idFraisForfait, quantite) VALUES ('"+idv+"', '"+date+"', '"+id+"', '"+quantite+"')"; 
			Cdao odaoBis = new Cdao();
			odaoBis.insertEnreg(req2);
			return null;
		    }
		return null;
		}


        public string  updateFraisForfait(string id, string idv, int quantite)
        {
            string date = Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Month);
            string req ="UPDATE lignefraisforfait SET quantite='"+quantite+"' WHERE mois='"+date+"' AND idFraisForfait='"+id+"' AND idVisiteur='"+idv+"'";
		    Cdao odao = new Cdao();
		    odao.update(req);
            return null;
        }


        public string deletFraisForfait (string id)
        {
            string date = Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Month);
            string req ="DELETE FROM lignefraisforfait WHERE idFraisForfait='"+id+"' AND mois='"+date+"'";
            Cdao odao = new Cdao();
		    odao.deleteEnreg(req);
		    return null;
        }
    }
}