﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using TP1_GSB_Authentification.mesClasses.b;

namespace TP1_GSB_Authentification.mesClasses
{
    public class CficheFrais
    {
        public string idVisiteur { get; set; }
        public string mois { get; set; }
        public int nbJustificatifs { get; set; }
        public int montantValide { get; set; }
        public DateTime dateModif { get; set; }
        public string idEtat { get; set; }

        public CficheFrais(string sidVisiteur, string smois, int snbJustificatifs, int smontantValide, DateTime sdateModif, string sidEtat)
        {
            idVisiteur = sidVisiteur;
            mois = smois;
            nbJustificatifs = snbJustificatifs;
            montantValide = smontantValide;
            dateModif = sdateModif;
            idEtat = sidEtat;
        }
    }
    public class CficheFraiss
    {
        public Dictionary<string, CficheFrais> ocollDicoficheFrais;

        public CficheFraiss()
        {
            ocollDicoficheFrais = new Dictionary<string, CficheFrais>();
            Cdao odao = new Cdao();
            string query = "SELECT * FROM `fichefrais`";
            MySqlDataReader ord = odao.getReader(query);
            //Cvisiteur ovisiteur  = ocollDicovisit["toto"];
            while (ord.Read())
            {
                CficheFrais oficheFrais = new CficheFrais(Convert.ToString(ord["idVisiteur"]), Convert.ToString(ord["mois"]), Convert.ToInt32(ord["nbJustificatifs"]), Convert.ToInt32(ord["montantValide"]), Convert.ToDateTime(ord["dateModif"]), Convert.ToString(ord["idEtat"]));
                ocollDicoficheFrais.Add(Convert.ToString(ord["idVisiteur"]), oficheFrais);
                ocollDicoficheFrais.Add(Convert.ToString(ord["mois"]), oficheFrais);
            }
        }
        public CficheFrais VerificationFicheFrais(string id)
        {
            string date = Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Month); //date("Y").date("m");

            CficheFrais oficheFrais;
            bool trouve = ocollDicoficheFrais.TryGetValue(id, out oficheFrais);
            if (trouve && date == oficheFrais.mois)
            {
                return oficheFrais;
            }
            else
            {
                CficheFraiss creation = new CficheFraiss();
                creation.creerFicheFrais(date, id); 
            }
            return null;
        }


        public string creerFicheFrais(string date, string id)
        {
            string req = "INSERT INTO fichefrais(idVisiteur, mois, dateModif) VALUES ('"+id+"', '"+date+"', now())";
            Cdao odao = new Cdao();
            odao.insertEnreg(req);
            CficheFraiss verification = new CficheFraiss();
            verification.VerificationFicheFrais(id);
            return null;
        }
    }
}
    
