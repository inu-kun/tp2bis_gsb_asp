﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using TP1_GSB_Authentification.mesClasses.b;

namespace TP1_GSB_Authentification.mesClasses
{
        public class Cvisiteur
        {
            public string id_visiteur { get; set; }
            public string nom_visiteur { get; set; }
            public string prenom_visiteur { get; set; }

            public string login_visiteur { get; set; }
            public string mdp_visiteur { get; set; }
            public string adresse_visiteur { get; set; }
            public int cp_visiteur { get; set; }
            public string ville_visiteur { get; set; }
            public DateTime dateEmbauche_visiteur { get; set; }


            public Cvisiteur(string sid_visiteur, string snom_visiteur, string sprenom_visiteur, string slogin_visiteur, string smdp_visiteur, string sadresse_visiteur, int scp_visiteur, string sville_visiteur, DateTime sdateEmbauche_visiteur)
            {
                id_visiteur = sid_visiteur;
                nom_visiteur = snom_visiteur;
                prenom_visiteur = sprenom_visiteur;
                login_visiteur = slogin_visiteur;
                mdp_visiteur = smdp_visiteur;
                adresse_visiteur = sadresse_visiteur;
                cp_visiteur = scp_visiteur;
                ville_visiteur = sville_visiteur;
                dateEmbauche_visiteur = sdateEmbauche_visiteur;
            }
        }

        public class Cvisiteurs
        {
            public Dictionary<string, Cvisiteur> ocollDicovisit;

            public Cvisiteurs()
            {
                ocollDicovisit = new Dictionary<string, Cvisiteur>();
                Cdao odao = new Cdao();
                string query = "SELECT * FROM visiteur";
                MySqlDataReader ord = odao.getReader(query);
                //Cvisiteur ovisiteur  = ocollDicovisit["toto"];
                while (ord.Read())
                {
                    Cvisiteur ovisiteur = new Cvisiteur(Convert.ToString(ord["id"]), Convert.ToString(ord["nom"]), Convert.ToString(ord["prenom"]), Convert.ToString(ord["login"]), Convert.ToString(ord["mdp"]), Convert.ToString(ord["adresse"]), Convert.ToInt32(ord["cp"]), Convert.ToString(ord["ville"]), Convert.ToDateTime(ord["dateEmbauche"]));

                    ocollDicovisit.Add(Convert.ToString(ord["login"]), ovisiteur);
                    ocollDicovisit.Add(Convert.ToString(ord["mdp"]), ovisiteur);
                }
            }

            public Cvisiteur getVisiteur(string sid, string smdp)
            {
                Cvisiteur ovisiteur;
                bool trouve = ocollDicovisit.TryGetValue(sid, out ovisiteur);
                if (trouve && smdp==ovisiteur.mdp_visiteur)
                {
                    return ovisiteur;
                }
                else
                {
                    return null;
                }

            }

            
        }
    }  